import epics
import json
import time
from numpy import linspace
from threading import Thread

class posScreen(Thread):
    def __init__(self, stop_signal):
        Thread.__init__(self)
        self.stop_signal = stop_signal

    def run(self):
        pvs = ['ISrc-010:ISS-Magtr:PulsHLvlS', 'ISrc-010:ISS-Magtr:Setup.B6', 'ISrc-010:PwrC-RepPS-01:PwrR', 'ISrc-010:PwrC-RepPS-01:VolR', 
                'LEBT-010:PwrC-RepPS-01:PwrR', 'LEBT-010:PwrC-RepPS-01:VolR', 'ISrc-010:ISS-HVPS:PwrR', 'ISrc-010:ISS-HVPS:VolR', 
                'ISrc-010:TS-EVG-01:Mxc1-Frequency-RB', 'ISrc-010:ISS-EVR-Magtr:Pul0-Width-RB', 'ISrc-010:Vac-VVMC-01100:FlwR',
                'LEBT-010:Vac-VVMC-01100:FlwR', 'ISrc-010:Vac-VVA-01100:OpenR', 'LEBT-010:Vac-VVA-01100:OpenR', 'LEBT-010:Vac-VVS-20000:ClosedR', 
                'ISrc-010:PwrC-CoilPS-01:CurR', 'ISrc-010:PwrC-CoilPS-02:CurR', 'ISrc-010:PwrC-CoilPS-03:CurR', 'ISrc-010:PwrC-CoilPS-01:PwrR', 
                'ISrc-010:PwrC-CoilPS-02:PwrR', 'ISrc-010:PwrC-CoilPS-03:PwrR', 'LEBT-010:PwrC-SolPS-01:PwrR', 'LEBT-010:PwrC-SolPS-01:CurR', 
                'LEBT-010:PwrC-SolPS-02:PwrR', 'LEBT-010:PwrC-SolPS-02:CurR', 'LEBT-010:PwrC-PSCH-01:PwrR', 'LEBT-010:PwrC-PSCH-01:CurR', 
                'LEBT-010:PwrC-PSCH-02:PwrR', 'LEBT-010:PwrC-PSCH-02:CurR', 'LEBT-010:PwrC-PSCV-01:PwrR', 'LEBT-010:PwrC-PSCV-01:CurR', 
                'LEBT-010:PwrC-PSCV-02:PwrR', 'LEBT-010:PwrC-PSCV-02:CurR', 'ISrc-010:ISS-EVR-Magtr:Pul1-Width-SP', 
                'NSO-LCR:Ops:Msg', 'NSO-LCR:Ops:SID', 'NSO-LCR:Ops:MSL', 'NSO-LCR:Ops:TSMsg', 'NSO-LCR:Ops:TSSL',
                'ISrc-010:PBI-BCM-001:AI4-Compressed', 'LEBT-010:PBI-BCM-001:AI5-Compressed', 
                'LEBT-020:PBI-FC-001:AMC31-AOI11-ArrayDataComp', 'LEBT-020:PBI-FC-001:MCU08-PositionR', 
                'ISrc-010:ISS-EVR-Magtr:Event-14-Cnt-I']

        epics_dict={}
        for pv in pvs:
            epics_dict[pv]=epics.PV(pv, auto_monitor=True)
       
        bcm_correction=15/8
 
        while(not self.stop_signal.isSet()):
            json_dict={}
            for pv in epics_dict:
                json_dict[pv]={}
                if epics_dict[pv].connected:
                    json_dict[pv]['units']=epics_dict[pv].units
                    if pv=='ISrc-010:PBI-BCM-001:AI4-Compressed':
                        y_data_array=epics_dict[pv].value*bcm_correction
                        y_data=list(y_data_array)
                        x_data_array=linspace(0,12.8,len(y_data))
                        x_data=list(x_data_array)
                        json_dict[pv]['value']=list(zip(x_data,y_data))
                        json_dict['pulse']={}
                        json_dict['max_curr']={}
                        json_dict['pulse']['units']='ms'
                        json_dict['max_curr']['units']='mA'
                        if y_data_array.max()<1:
                            json_dict['beam']=2
                            json_dict['pulse']['value']='0'
                            json_dict['max_curr']['value']='0'
                        else:
                            threshold=max(y_data_array)*0.25
                            json_dict['pulse']['value']=round(x_data_array[y_data_array>threshold].max()-x_data_array[y_data_array>threshold].min(),2)
                            json_dict['max_curr']['value']=round(max(y_data_array),2)
                            if epics_dict['ISrc-010:ISS-Magtr:Setup.B6'].value and epics_dict['ISrc-010:ISS-HVPS:PwrR'].value:
                                json_dict['beam']=0
                            else:
                                json_dict['beam']=1
                        continue
                    if pv=='LEBT-010:PBI-BCM-001:AI5-Compressed':
                        y_data=list(epics_dict[pv].value)
                        x_data=list(linspace(0,12.8,len(y_data)))
                        json_dict[pv]['value']=list(zip(x_data,y_data))
                        continue
                    if pv=='LEBT-020:PBI-FC-001:AMC31-AOI11-ArrayDataComp':
                        fc_resizer = int(len(epics_dict[pv].value)/200)
                        y_data=list(epics_dict[pv].value[::fc_resizer])
                        x_data=list(linspace(0,10,len(y_data)))
                        json_dict[pv]['value']=list(zip(x_data,y_data))
                        continue
                    try:
                        json_dict[pv]['value']=round(epics_dict[pv].value,3)
                    except:
                        json_dict[pv]['value']=epics_dict[pv].value
                else:
                    json_dict[pv]['units']='n.c.'
                    json_dict[pv]['value']=''
            try:
                json_dict['ISrc-010:TS-EVG-01:Mxc1-Frequency-RB']['value']=int(json_dict['ISrc-010:TS-EVG-01:Mxc1-Frequency-RB']['value'])
                json_dict['ISrc-010:ISS-EVR-Magtr:Pul0-Width-RB']['value']=int(json_dict['ISrc-010:ISS-EVR-Magtr:Pul0-Width-RB']['value'])
            except:
                json_dict['ISrc-010:TS-EVG-01:Mxc1-Frequency-RB']['value']=''
                json_dict['ISrc-010:ISS-EVR-Magtr:Pul0-Width-RB']['value']=''

            tmp_json = json.dumps(json_dict)
            tmp_json = tmp_json.replace('NaN','0')
            tmp_json = tmp_json.replace('Infinity','0')
            with open('/var/www/data/pos.json','w') as datafile:
                datafile.write(tmp_json)
            time.sleep(0.5)

        for pv in epics_dict:
            epics_dict[pv].clear_auto_monitor()
            epics_dict[pv].disconnect()
