import epics
import json
import time
import os
import matplotlib.pyplot as plt
from numpy import linspace, zeros
from threading import Thread

class mybeautifulScreen(Thread):
    def __init__(self, stop_signal):
        Thread.__init__(self)
        self.stop_signal = stop_signal

    def run(self):
        pvs = ['PV-NUMBER', 'PV-ARRAY', 'PV-IMAGE'] # Define the PV list

        epics_dict={}
        for pv in pvs:
            epics_dict[pv]=epics.PV(pv, auto_monitor=True)

        while(not self.stop_signal.isSet()):
            json_dict={}
            for pv in epics_dict:
                json_dict[pv]={}
                if epics_dict[pv].connected:

                    # Example of Array
                    if pv=='PV-ARRAY': # The arrays need a list made by pairs of X,Y
                        y_data=list(epics_dict[pv].value)
                        x_data=list(linspace(0,10,len(y_data))) # in this case the X axis will go from 0 to 10
                        json_dict[pv]['value']=list(zip(x_data,y_data))
                        continue

                    # Example of Image
                    if pv=='PV-IMAGE': # We need to create the PNG
                        try:
                            imarray = epics_dict[PV-IMAGE].value.reshape(154,206) # From the array I reshape into the size of the image
                        except:
                            imarray = zeros((154,206)) # If I don't get the correct data I do a blank image
                    plt.imsave('/var/www/data/tmp-my_image.png', imarray, cmap='jet') # Save as tmp
                    os.rename('/var/www/data/tmp-my_image.png', '/var/www/data/my_image.png') # Swap the tmp image with the final one. This trick is used to avoid blinking image in the final page.

                    json_dict[pv]['units']=epics_dict[pv].units # I set the units for all the PVs
                    try:
                        json_dict[pv]['value']=round(epics_dict[pv].value,3) # Where possible I round the value to three decimal digits
                    except:
                        json_dict[pv]['value']=epics_dict[pv].value
                else:
                    json_dict[pv]['units']='n.c.' # If the PV is disconnected I assign n.c. as unit an blank value
                    json_dict[pv]['value']=''
  
            tmp_json = json.dumps(json_dict) # Create the JSON from dictionary
            tmp_json = tmp_json.replace('NaN','0') # Replace the occurences of NaN with 0
            tmp_json = tmp_json.replace('Infinity','0') # Replace the infinities with 0
            with open('/var/www/data/my_file.json','w') as datafile: # Write the final file
                datafile.write(tmp_json)
            time.sleep(0.5) # Refresh every half a second
          
        for pv in epics_dict: # When the stop signal is set, it cancels all the auto monitor and disconnects
            epics_dict[pv].clear_auto_monitor()
            epics_dict[pv].disconnect()
