import epics
import json
import time
import os
import matplotlib.pyplot as plt
from numpy import linspace, zeros, tile
from threading import Thread

class instrumentsScreen(Thread):
    def __init__(self, stop_signal):
        Thread.__init__(self)
        self.stop_signal = stop_signal

    def run(self):

        pvs = [ 'LEBT-010:PBI-NPM-001:HCAMSLOWIMG-ArrayData', 'LEBT-010:PBI-NPM-002:VCAMSLOWIMG-ArrayData', 'LEBT-010:PBI-NPM-001:HCAMFITS-Y_RBV', 'LEBT-010:PBI-NPM-002:VCAMFITS-Y_RBV', 'LEBT-010:PBI-NPM-001:HCAM-SCALEFACT', 'LEBT-010:PBI-NPM-002:VCAM-SCALEFACT', 'LEBT-020:PBI-NPM-001:HCAMSLOWIMG-ArrayData', 'LEBT-020:PBI-NPM-002:VCAMSLOWIMG-ArrayData', 'LEBT-020:PBI-NPM-001:HCAMFITS-Y_RBV', 'LEBT-020:PBI-NPM-002:VCAMFITS-Y_RBV', 'LEBT-020:PBI-NPM-001:HCAM-SCALEFACT', 'LEBT-020:PBI-NPM-002:VCAM-SCALEFACT', 'ISrc-010:PBI-BCM-001:AI4-Compressed', 'LEBT-010:PBI-BCM-001:AI5-Compressed', 'LEBT-020:PBI-FC-001:AMC31-AOI11-ArrayDataComp', 'LEBT-020:PBI-FC-001:MCU08-PositionR', 'LNS-ISRC-010:PBI-EMV:BUFF-CURRENT', 'LNS-ISRC-010:PBI-EMV:MTR.NPTS', 'LNS-ISRC-010:PBI-EMV:PS.NPTS', 'LNS-ISRC-010:PBI-EMH:BUFF-CURRENT', 'LNS-ISRC-010:PBI-EMH:MTR.NPTS', 'LNS-ISRC-010:PBI-EMH:PS.NPTS', 'LEBT-010:PBI-Dpl-001:IMG1-ArrayData', 'LEBT-010:PBI-Dpl-001:FIT1-Y_RBV', 'LEBT-010:PBI-Dpl-001:SPECT-MinWavelength', 'LEBT-010:PBI-Dpl-001:SPECT-MaxWavelength', 'LEBT-010:PBI-Dpl-001:ROI1-MinX', 'LEBT-010:PBI-Dpl-001:ROI1-SizeX', 'LEBT-010:PBI-Dpl-001:FIT1-FracProton', 'LEBT-010:PBI-Dpl-001:FIT1-FracH2', 'LEBT-010:PBI-Dpl-001:FIT1-FracH3']

        epics_dict={}
        for pv in pvs:
            epics_dict[pv]=epics.PV(pv, auto_monitor=True)

        while(not self.stop_signal.isSet()):
            try:
                if epics_dict['LEBT-010:PBI-NPM-002:VCAMSLOWIMG-ArrayData'].connected:
                    imarray = epics_dict['LEBT-010:PBI-NPM-002:VCAMSLOWIMG-ArrayData'].value.reshape(154,206)
                else:
                    imarray = zeros((154,206))
            except:
                imarray = zeros((154,206))
            plt.imsave('/var/www/data/tmp-npm-vimage.png', imarray, cmap='jet')
            os.rename('/var/www/data/tmp-npm-vimage.png', '/var/www/data/npm-vimage.png')

            try:
                if epics_dict['LEBT-010:PBI-NPM-001:HCAMSLOWIMG-ArrayData'].connected:
                    imarray = epics_dict['LEBT-010:PBI-NPM-001:HCAMSLOWIMG-ArrayData'].value.reshape(154,206)
                else:
                    imarray = zeros((154,206))
            except:
                imarray = zeros((154,206))
            plt.imsave('/var/www/data/tmp-npm-himage.png', imarray, cmap='jet')
            os.rename('/var/www/data/tmp-npm-himage.png', '/var/www/data/npm-himage.png')

            try:
                if epics_dict['LEBT-020:PBI-NPM-002:VCAMSLOWIMG-ArrayData'].connected:
                    imarray = epics_dict['LEBT-020:PBI-NPM-002:VCAMSLOWIMG-ArrayData'].value.reshape(154,206)
                else:
                    imarray = zeros((154,206))
            except:
                imarray = zeros((154,206))
            plt.imsave('/var/www/data/tmp-npm-commissioning-vimage.png', imarray, cmap='jet')
            os.rename('/var/www/data/tmp-npm-commissioning-vimage.png', '/var/www/data/npm-commissioning-vimage.png')

            try:
                if epics_dict['LEBT-020:PBI-NPM-001:HCAMSLOWIMG-ArrayData'].connected:
                    imarray = epics_dict['LEBT-020:PBI-NPM-001:HCAMSLOWIMG-ArrayData'].value.reshape(154,206)
                else:
                    imarray = zeros((154,206))
            except:
                imarray = zeros((154,206))
            plt.imsave('/var/www/data/tmp-npm-commissioning-himage.png', imarray, cmap='jet')
            os.rename('/var/www/data/tmp-npm-commissioning-himage.png', '/var/www/data/npm-commissioning-himage.png')

            try:            
                if epics_dict['LNS-ISRC-010:PBI-EMV:BUFF-CURRENT'].connected:
                    pos = epics_dict['LNS-ISRC-010:PBI-EMV:MTR.NPTS'].value
                    ang = epics_dict['LNS-ISRC-010:PBI-EMV:PS.NPTS'].value
                    imarray = (epics_dict['LNS-ISRC-010:PBI-EMV:BUFF-CURRENT'].value)[:pos*ang].reshape((ang,pos))
                    imarray=(imarray*255)
                else:
                    imarray = zeros((200,200))
            except:
                imarray = zeros((200,200))
            plt.imsave('/var/www/data/tmp-emu-vimage.png', imarray, cmap='jet')
            os.rename('/var/www/data/tmp-emu-vimage.png', '/var/www/data/emu-vimage.png')

            try:            
                if epics_dict['LNS-ISRC-010:PBI-EMH:BUFF-CURRENT'].connected:
                    pos = epics_dict['LNS-ISRC-010:PBI-EMH:MTR.NPTS'].value
                    ang = epics_dict['LNS-ISRC-010:PBI-EMH:PS.NPTS'].value
                    imarray = (epics_dict['LNS-ISRC-010:PBI-EMH:BUFF-CURRENT'].value)[:pos*ang].reshape((ang,pos))
                    imarray=(imarray*255)
                else:
                    imarray = zeros((200,200))
            except:
                imarray = zeros((200,200))
            plt.imsave('/var/www/data/tmp-emu-himage.png', imarray, cmap='jet')
            os.rename('/var/www/data/tmp-emu-himage.png', '/var/www/data/emu-himage.png')

            json_dict={}
            for pv in epics_dict:
                json_dict[pv]={}
                if epics_dict[pv].connected:
                    json_dict[pv]['units']=epics_dict[pv].units
                    if pv=='ISrc-010:PBI-BCM-001:AI4-Compressed':
                        y_data=list(epics_dict[pv].value*bcm_correction)
                        x_data=list(linspace(0,12.8,len(y_data)))
                        json_dict[pv]['value']=list(zip(x_data,y_data))
                        continue
                    if pv=='LEBT-010:PBI-BCM-001:AI5-Compressed':
                        y_data=list(epics_dict[pv].value)
                        x_data=list(linspace(0,12.8,len(y_data)))
                        json_dict[pv]['value']=list(zip(x_data,y_data))
                        continue
                    if pv=='LEBT-020:PBI-FC-001:AMC31-AOI11-ArrayDataComp':
                        fc_resizer = int(len(publicpvs[pv].value)/200)
                        y_data=list(epics_dict[pv].value[::fc_resizer])
                        x_data=list(linspace(0,10,len(y_data)))
                        json_dict[pv]['value']=list(zip(x_data,y_data))
                        continue
                    if pv=='LEBT-010:PBI-NPM-001:HCAMFITS-Y_RBV':
                        y_data=list(epics_dict[pv].value)
                        x_size=len(y_data)*epics_dict['LEBT-010:PBI-NPM-001:HCAM-SCALEFACT'].value/1000
                        x_data=list(linspace(-x_size/2,x_size/2,len(y_data)))
                        json_dict[pv]['value']=list(zip(x_data,y_data))
                        continue
                    if pv=='LEBT-010:PBI-NPM-002:VCAMFITS-Y_RBV':
                        y_data=list(epics_dict[pv].value)
                        x_size=len(y_data)*epics_dict['LEBT-010:PBI-NPM-002:VCAM-SCALEFACT'].value/1000
                        x_data=list(linspace(-x_size/2,x_size/2,len(y_data)))
                        json_dict[pv]['value']=list(zip(x_data,y_data))
                        continue
                    if pv=='LEBT-020:PBI-NPM-001:HCAMFITS-Y_RBV':
                        y_data=list(epics_dict[pv].value)
                        x_size=len(y_data)*epics_dict['LEBT-020:PBI-NPM-001:HCAM-SCALEFACT'].value/1000
                        x_data=list(linspace(-x_size/2,x_size/2,len(y_data)))
                        json_dict[pv]['value']=list(zip(x_data,y_data))
                        continue
                    if pv=='LEBT-020:PBI-NPM-002:VCAMFITS-Y_RBV':
                        y_data=list(epics_dict[pv].value)
                        x_size=len(y_data)*epics_dict['LEBT-020:PBI-NPM-002:VCAM-SCALEFACT'].value/1000
                        x_data=list(linspace(-x_size/2,x_size/2,len(y_data)))
                        json_dict[pv]['value']=list(zip(x_data,y_data))
                        continue
                    if pv=='LEBT-010:PBI-NPM-001:HCAM-SCALEFACT':
                        json_dict[pv]['units']='n.c.'
                        json_dict[pv]['value']=''
                        continue
                    if pv=='LEBT-010:PBI-NPM-002:VCAM-SCALEFACT':
                        json_dict[pv]['units']='n.c.'
                        json_dict[pv]['value']=''
                        continue
                    if pv=='LEBT-020:PBI-NPM-001:HCAM-SCALEFACT':
                        json_dict[pv]['units']='n.c.'
                        json_dict[pv]['value']=''
                        continue
                    if pv=='LEBT-020:PBI-NPM-002:VCAM-SCALEFACT':
                        json_dict[pv]['units']='n.c.'
                        json_dict[pv]['value']=''
                        continue

                    try:            
                        if epics_dict['LEBT-010:PBI-Dpl-001:IMG1-ArrayData'].connected:
                            start_image = epics_dict['LEBT-010:PBI-Dpl-001:ROI1-MinX'].value
                            end_image = start_image + epics_dict['LEBT-010:PBI-Dpl-001:ROI1-SizeX'].value
                            imarray = tile(epics_dict['LEBT-010:PBI-Dpl-001:IMG1-ArrayData'].value[start_image:end_image], (256,1))
                        else:
                            imarray = zeros((256,512))
                    except:
                        imarray = zeros((256,512))
                    plt.imsave('/var/www/data/tmp-dpl.png', imarray, cmap='hot')
                    os.rename('/var/www/data/tmp-dpl.png', '/var/www/data/dpl.png')

                    if pv=='LEBT-010:PBI-Dpl-001:FIT1-Y_RBV':
                        y_data=list(epics_dict[pv].value)
                        pixel2lambda=(epics_dict['LEBT-010:PBI-Dpl-001:SPECT-MaxWavelength'].value-epics_dict['LEBT-010:PBI-Dpl-001:SPECT-MinWavelength'].value)/1024
                        start_wave = epics_dict['LEBT-010:PBI-Dpl-001:SPECT-MinWavelength'].value + epics_dict['LEBT-010:PBI-Dpl-001:ROI1-MinX'].value*pixel2lambda
                        end_wave = epics_dict['LEBT-010:PBI-Dpl-001:SPECT-MinWavelength'].value + (epics_dict['LEBT-010:PBI-Dpl-001:ROI1-MinX'].value+epics_dict['LEBT-010:PBI-Dpl-001:ROI1-SizeX'].value)*pixel2lambda
                        x_data=list(linspace(start_wave, end_wave,  len(y_data)))
                        json_dict[pv]['value']=list(zip(x_data,y_data))
                        continue
        
                    if epics_dict[pv].count>1 or epics_dict[pv].count==0:
                        continue
                    try:
                        json_dict[pv]['value']=round(epics_dict[pv].value,3)
                    except:
                        json_dict[pv]['value']=epics_dict[pv].value
                else:
                    json_dict[pv]['units']='n.c.'
                    json_dict[pv]['value']=''

            tmp_json = json.dumps(json_dict)
            tmp_json = tmp_json.replace('NaN','0')
            tmp_json = tmp_json.replace('Infinity','0')
            with open('/var/www/data/instruments.json','w') as datafile:
                datafile.write(tmp_json)

            time.sleep(0.5)

        for pv in epics_dict:
            epics_dict[pv].clear_auto_monitor()
            epics_dict[pv].disconnect()

