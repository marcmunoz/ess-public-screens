import epics
import json
import time
from threading import Thread

class interlocksScreen(Thread):
    def __init__(self, stop_signal):
        Thread.__init__(self)
        self.stop_signal = stop_signal

    def run(self):
        pvs = ['ISrc-010:ISS-Intf-HMI:ComIocOK', 'ISrc-010:ISS-Intf-PSS:HVPermitR', 'ISrc-010:ISS-HVPf-Res:CloseR', 'ISrc-010:ISS-HVPf-Doors:CloseR', 'ISrc-010:ISS-Intf-Vac:VacPermit', 'LEBT-010:PBI-FC-001:WtrFlowOK', 'LEBT-010:PBI-EMU-001:VerWtrFlowOK', 'LEBT-010:PBI-EMU-001:HorWtrFlowOK', 'ISrc-010:PwrC-HVIT-01:LidOpenR', 'ISrc-010:PwrC-HVIT-01:OilTempOK', 'ISrc-010:ISS-HVPS:PwrR.SEVR', 'ISrc-010:ISS-Intf-HMI:ESTOP_MEMO', 'ISrc-010:ISS-HVPS:ITLckEnCmdR', 'ISrc-010:ISS-Intf-HMI:EVENT_B_MEMO'] 

        epics_dict={}
        for pv in pvs:
            epics_dict[pv]=epics.PV(pv, auto_monitor=True)

        while(not self.stop_signal.isSet()):
            json_dict={}
            for pv in epics_dict:
                json_dict[pv]={}
                if epics_dict[pv].connected:
                    json_dict[pv]['units']=epics_dict[pv].units
                    try:
                        json_dict[pv]['value']=round(epics_dict[pv].value,3)
                    except:
                        json_dict[pv]['value']=epics_dict[pv].value
                else:
                    json_dict[pv]['units']='n.c.'
                    json_dict[pv]['value']=''
  
            tmp_json = json.dumps(json_dict)
            tmp_json = tmp_json.replace('NaN','0')
            tmp_json = tmp_json.replace('Infinity','0')
            with open('/var/www/data/interlocks.json','w') as datafile:
                datafile.write(tmp_json)
            time.sleep(0.5)
          
        for pv in epics_dict:
            epics_dict[pv].clear_auto_monitor()
            epics_dict[pv].disconnect()
