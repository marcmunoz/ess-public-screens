import epics
import json
import time
from threading import Thread

class ts2Screen(Thread):
    def __init__(self, stop_signal):
        Thread.__init__(self)
        self.stop_signal = stop_signal

    def run(self):
        pvs = ['CrS-TICP:Cryo-TE-31491:Val', 'TS2-010CDL:Cryo-TE-82313:MeasValue', 'TS2-010CDL:Cryo-TE-82360:MeasValue', 
              'TS2-010CRM:Cryo-TE-068:MeasValue', 'TS2-010CRM:Cryo-TE-063:MeasValue', 'TS2-010CRM:Cryo-TE-069:MeasValue', 
              'TS2-010CRM:Cryo-TE-064:MeasValue', 'TS2-010CDL:Cryo-TE-82365:MeasValue', 'TS2-010CDL:Cryo-TE-82314:MeasValue', 
              'CrS-TICP:Cryo-TE-31492:Val',
              'NSO-LCR:Ops:TSMsg', 'NSO-LCR:Ops:TSSL',
              ]

        epics_dict={}
        for pv in pvs:
            epics_dict[pv]=epics.PV(pv, auto_monitor=True)

        while(not self.stop_signal.isSet()):
            json_dict={}
            for pv in epics_dict:
                json_dict[pv]={}
                if epics_dict[pv].connected:
                    json_dict[pv]['units']=epics_dict[pv].units
                    try:
                        json_dict[pv]['value']=round(epics_dict[pv].value,3)
                    except:
                        json_dict[pv]['value']=epics_dict[pv].value
                else:
                    json_dict[pv]['units']='n.c.'
                    json_dict[pv]['value']=''
  
            tmp_json = json.dumps(json_dict)
            tmp_json = tmp_json.replace('NaN','0')
            tmp_json = tmp_json.replace('Infinity','0')
            with open('/var/www/data/ts2.json','w') as datafile:
                datafile.write(tmp_json)
            time.sleep(0.5)
          
        for pv in epics_dict:
            epics_dict[pv].clear_auto_monitor()
            epics_dict[pv].disconnect()
