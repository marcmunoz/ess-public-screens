import epics
import time
import json
import os
import signal
import matplotlib.pyplot as plt
from git import Repo
from shutil import rmtree
from glob import glob
from numpy import linspace, array, zeros, flip, rot90, tile
from http.server import BaseHTTPRequestHandler,HTTPServer
from epicsarchiver import ArchiverAppliance
from threading import Thread, Event
from screens.pos import posScreen
from screens.interlocks import interlocksScreen
from screens.instruments import instrumentsScreen
from screens.ts2 import ts2Screen

PORT_NUMBER = 8080
ARCHIVER_SERVER = os.environ.get('ARCHIVER_SERVER', 'archiver-01.tn.esss.lu.se')
archiver = ArchiverAppliance(ARCHIVER_SERVER)
pv_repo = os.environ.get('PV_REPO', 'https://bitbucket.org/europeanspallationsource/isrc-lebt-pv-list.git')
stop_signal = Event()
stop_signal.clear()

bcm_correction=15/8

try:
    Repo.clone_from(pv_repo, './repo')
    pvfiles=glob('./repo/*.txt')
except:
    print('PV Repository not available')
    quit()

def save_json(json_data, outfile):
    tmp_json = json.dumps(json_data)
    tmp_json = tmp_json.replace('NaN','0')
    tmp_json = tmp_json.replace('Infinity','0')
    with open(outfile,'w') as datafile:
        datafile.write(tmp_json)
    return

pvlist=[]
for filename in pvfiles:
    sheet = filename.split('/')[2].split('.')[0]
    data=open(filename,'r')
    pvlist+=data.read().splitlines()
    data.close()
rmtree('./repo')
pvlist.sort()

global_dict={}
for PV in pvlist:
    try:
        sheet=PV.split(':')[0]
        column=PV.split(':')[1]
        if sheet not in global_dict.keys():
            global_dict[sheet]={}
        if column not in global_dict[sheet].keys():
            global_dict[sheet][column]={}
        global_dict[sheet][column][PV]=False
    except:
        None

save_json(global_dict, '/var/www/data/getData.json')


class myHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        path = list(filter(None,self.path.split('/')))
        if len(path) == 2:
            if path[0] == 'search':
                returnlist=''
                self.send_header('Content-type','application/json')
                self.send_header('Access-Control-Allow-Origin', '*')
                self.send_header('Access-Control-Allow-Headers', 'Authorization, Content-Type')
                self.end_headers()
                wordlist=path[1].split("%20")
                if len(wordlist)<=1 and len(wordlist[0])<2:
                    returnlist=''
                    return
                result=pvlist
                for word in wordlist:
                    result=[row for row in result if word.lower() in row.lower()]
                if len(result)==0:
                    returnlist='No PV found.'
                else:
                    for pv in result:
                        returnlist+='<a href="#" onclick="searchPV(\''+pv+'\')">'+pv+'</a>'
                        returnlist+='<br>'
                self.wfile.write(bytes(json.dumps({'pvs':returnlist}), "utf-8"))
                return

            if path[0] == 'getPVHTML':
                self.send_header('Content-type','application/json')
                self.send_header('Access-Control-Allow-Origin', '*')
                self.send_header('Access-Control-Allow-Headers', 'Authorization, Content-Type')
                self.end_headers()
                pvinfo={}
                try:
                    myPV=epics.PV(path[1])
                    pvinfo['info']=myPV.info.replace('\n','<br>')
                    myPV.clear_auto_monitor()
                    myPV.disconnect()
                    archiver_stat = ''
                    archiver_dict=archiver.get_pv_status(pv=path[1])[0]
                    for i in archiver_dict:
                        archiver_stat = archiver_stat + i + ' = '+ archiver_dict[i]+'\n'
                    pvinfo['archiver']=archiver_stat.replace('\n','<br>')
                    if archiver_dict['status'] == 'Being archived':
                        pvinfo['link']='<a target="_blank" href="/plot/retrieval/ui/viewer/archViewer.html?pv='+path[1]+'">See archived data</a>'
                    self.wfile.write(bytes(json.dumps(pvinfo), "utf-8"))
                    return
                except:
                    pvinfo['info']="The PV seems disconnected"
                    pvinfo['archiver']=''
                    self.wfile.write(bytes(json.dumps(pvinfo), "utf-8"))
                    return

            if path[0] == 'getPV':
                self.send_header('Content-type','application/json')
                self.send_header('Access-Control-Allow-Origin', '*')
                self.send_header('Access-Control-Allow-Headers', 'Authorization, Content-Type')
                self.end_headers()
                pvinfo={}
                try:
                    if path[1]=='ISrc-010:PBI-BCM-001:AI4-Compressed-Mean':
                        myPV=epics.PV('ISrc-010:PBI-BCM-001:AI4-Compressed', auto_monitor=True)
                        mean_value=myPV.value.mean()*bcm_correction
                        tmp_info = myPV.info.split('\n')
                        tmp_info[0]='== ISrc-010:PBI-BCM-001:AI4-Compressed-Mean  (time_double) =='
                        tmp_info[1]='   value      = '+str(mean_value)
                        tmp_info[2]="   char_value = '"+str(mean_value)+"'"
                        tmp_info[3]='   count      = 1'
                        tmp_info[4]='   nelm       = 1'
                        new_info=''
                        for i in tmp_info:
                            new_info+=i
                            new_info+='\n'
                        new_info=new_info[:-1]
                        myPV.clear_auto_monitor()
                        myPV.disconnect()
                        pvinfo['info']=new_info
                        pvinfo['archiver']="pvName = ISrc-010:PBI-BCM-001:AI4-Compressed-Mean\nstatus = Not being archived\n"
                        pvinfo['live_view']="ISrc-010:PBI-BCM-001:AI4-Compressed"
                        self.wfile.write(bytes(json.dumps(pvinfo), "utf-8"))
                        return
                    if path[1]=='ISrc-010:PBI-BCM-001:AI4-Compressed-Max':
                        myPV=epics.PV('ISrc-010:PBI-BCM-001:AI4-Compressed', auto_monitor=True)
                        max_value=myPV.value.max()*bcm_correction
                        tmp_info = myPV.info.split('\n')
                        tmp_info[0]='== ISrc-010:PBI-BCM-001:AI4-Compressed-Max  (time_double) =='
                        tmp_info[1]='   value      = '+str(max_value)
                        tmp_info[2]="   char_value = '"+str(max_value)+"'"
                        tmp_info[3]='   count      = 1'
                        tmp_info[4]='   nelm       = 1'
                        new_info=''
                        for i in tmp_info:
                            new_info+=i
                            new_info+='\n'
                        new_info=new_info[:-1]
                        myPV.clear_auto_monitor()
                        myPV.disconnect()
                        pvinfo['info']=new_info
                        pvinfo['archiver']="pvName = ISrc-010:PBI-BCM-001:AI4-Compressed-Max\nstatus = Not being archived\n"
                        pvinfo['live_view']="ISrc-010:PBI-BCM-001:AI4-Compressed"
                        self.wfile.write(bytes(json.dumps(pvinfo), "utf-8"))
                        return

                    myPV=epics.PV(path[1])
                    pvinfo['info']=myPV.info
                    myPV.clear_auto_monitor()
                    myPV.disconnect()
                    archiver_stat = ''
                    archiver_dict=archiver.get_pv_status(pv=path[1])[0]
                    for i in archiver_dict:
                        archiver_stat = archiver_stat + i + ' = '+ archiver_dict[i]+'\n'
                    pvinfo['archiver']=archiver_stat
                    pvinfo['live_view']='https://live.pos.esss.lu.se/live_view?PV='+path[1]
                    if archiver_dict['status'] == 'Being archived':
                        pvinfo['link']='/plot/retrieval/ui/viewer/archViewer.html?pv='+path[1]
                    self.wfile.write(bytes(json.dumps(pvinfo), "utf-8"))
                    return
                except:
                    pvinfo['info']="Something wrong here, please retry"
                    pvinfo['archiver']="Something wrong here, please retry"
                    self.wfile.write(bytes(json.dumps(pvinfo), "utf-8"))
                    return
            return

        self.wfile.write(bytes('The server is running', "utf-8"))
        return


def on_exit(signum, frame):
    print(f'Signal handler called with signal {signum}')
    raise SystemExit("Exiting")

signal.signal(signal.SIGTERM, on_exit)

try:
    posThread = posScreen(stop_signal)
    posThread.start()
    interlocksThread = interlocksScreen(stop_signal)
    interlocksThread.start()
    instrumentsThread = instrumentsScreen(stop_signal)
    instrumentsThread.start()
    ts2Thread = ts2Screen(stop_signal)
    ts2Thread.start()
    
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print ('Started httpserver on port ' , PORT_NUMBER)
    server.serve_forever()

except (KeyboardInterrupt, SystemExit):
    print ('\n^C received, shutting down the web server')
    server.socket.close()
    stop_signal.set()
